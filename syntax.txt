(* cwhitespace is taken out in a previous pass *)
whitespace = " " | "\t";
program = { statement, [comment], newline };
statement = label | constant | db_directive | implied_operation | unary_operation | "";

label = identifier, ":";
constant = "const", identifier, "=", expression;
db_directive = "db" { db_expresion, "," };

implied_operation = implied_instruction, [ condition ];
implied_instruction = "hlt" | "ret" | "clc" | "clz" | "sec" | "sez";

unary_operation = unary_instruction, operand, [ condition ];
unary_instruction = "add" | "adc" | "sbw" | "swb" | "nnd" | "and" | "aib" | "anb"
        | "bia" | "bna" | "ora" | "nor" | "jmp" | "jsr" | "dec" | "inc" | "xor"
        | "xnr";

binary_operation = (cmp_operation | sub_operation | sbb_operation | mov_operation) [condition];

cmp_operation
        = "cmp", "ac", operand
        | "cmp", operand, "ac";

sub_operation
        = "sub", "ac", operand
        | "sub", operand, "ac";

sbb_operation
        = "sbb", "ac", operand
        | "sbb", operand, "ac";

mov_operation = "mov", operand, ",", operand
        | "mov", direct_register, ",", "(", operand, ["+" "ix"], ")"
        | "mov", "(", operand, ["+", "ix"], ")", ",", direct_register;

condition = "-", ["n"], ["c"], ["z"];

direct_register = "ac" | "br" | "ix" | "sp";
operand = direct_register
        | "stack"
        | immediate
        | "(", immediate, ["+", "ix"], ")";
immediate = expression;


db_expresion = expression | string_literal;
string_literal = '"', { string_char }, '"';
string_char = all_characters - (newline | '"' | '\')
            | '\', ('\' | '"' | "0" | "t" | "n" | "r");

expression = integer_literal | identifier;

integer_literal = decimal_literal | binary_literal | octal_literal | hexadecimal_literal;
decimal_literal = digit, { digit };
binary_literal = "0b", binary_digit, { binary_digit };
octal_literal = "0o", octal_digit, { octal_digit };
hexadecimal_literal = "0x", hexadecimal_digit, { hexadecimal_digit };

identifier = (letter | "_"), { digit | letter | "_" };

binary_digit = "0" | "1";
octal_digit = "0" | "1" | "2" | "3" | "4" | "5" | "6" | "7";
digit = "0" | "1" | "2" | "3" | "4" | "5" | "6" | "7" | "8" | "9";
hexadecimal_digit = digit | "a" | "b" | "c" | "d" | "e" | "f";
letter = ? a-z and A-Z ?;
newline = "\n";
all_characters = ? all characters ?;